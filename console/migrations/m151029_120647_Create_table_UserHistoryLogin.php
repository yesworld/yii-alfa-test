<?php

use yii\db\Migration;

class m151029_120647_Create_table_UserHistoryLogin extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_history_login}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(6)->notNull(),
            'ip' => $this->string(30)->notNull(),
            'timestamp' => $this->integer(11)->notNull()
        ], $tableOptions);
    }


    public function down()
    {
        $this->dropTable('{{%user_history_login}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

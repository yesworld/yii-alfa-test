<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_history_login".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $ip
 * @property integer $timestamp
 */
class UserHistoryLogin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_history_login';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ip', 'timestamp'], 'required'],
            [['user_id', 'timestamp'], 'integer'],
            [['ip'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'user_id' => 'Номер пользователя',
            'ip' => 'Ip Адрес',
            'timestamp' => 'Время',
        ];
    }
}

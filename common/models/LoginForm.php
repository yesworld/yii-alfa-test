<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $login;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // login and password are both required
            [['login', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Не правильно введен login и/или password. ');
            }
        }
    }

    //Атрибуты для формы
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить'
        ];
    }

    /**
     * Logs in a user using the provided login and password.
     *  And save User login history
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $User = $this->getUser();
            $isLogged = Yii::$app->user->login($User, $this->rememberMe ? 3600 * 24 * 30 : 0);

            if( $isLogged ){
                self::saveHistoryLog( $User );
            }

            return $isLogged;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[login]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->login);
        }

        return $this->_user;
    }

    public static function saveHistoryLog( $User )
    {

        $req = new yii\web\Request();

        $History = new UserHistoryLogin();
        $History->attributes = [
            'user_id' => $User->id,
            'ip' => $req->getUserIP(),
            'timestamp' => time()
        ];

        $History->save();

    }
}

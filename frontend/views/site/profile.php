<?php

/* @var $this yii\web\View */

$this->title = 'Личный кабинет';
use yii\helpers\Url;
use yii\grid\GridView;

//echo '<pre>';
//print_r( $dataProvider->history );
?>
<div class="site-index">

    <div class="jumbotron">
        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute('site/change-password'); ?>">Сменить пароль</a></p>
    </div>
<!--    -->

    <div class="body-content">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>История входа</h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                        Кол-во входа:
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-10 col-lg-10">
                        <?=$dataProvider->totalCount; ?>
                    </div>
                </div>

                <h4>Список IP адресов с датой и временем, с которых осуществлялся вход</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                [
                                    'attribute'=> "ip",
                                    "label"    => "ip"
                                ],
                                [
                                    'attribute'=> "timestamp",
                                    "label"    => "Время входа",
                                    "format"   => ['date',"php:d/m/Y H:m"]
                                ]

                            ],
                        ]); ?>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

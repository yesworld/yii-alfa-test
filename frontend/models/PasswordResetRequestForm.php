<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Обязательное поле для заполнения'],
            ['email', 'email', 'message' => 'Не верный формат почты'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'Такой почты нет'
            ],

            ['verifyCode', 'captcha', 'message' => 'Неверная каптча'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Логин/Емаил',
            'verifyCode' => 'Каптча',
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            [
                'OR',
                [ 'login' => $this->email ],
                [ 'email' => $this->email ]
            ],
            'status' => User::STATUS_ACTIVE
        ]);

        if ($user) {
            $user->createNewPassword();

            if ($user->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo( $user->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }

}

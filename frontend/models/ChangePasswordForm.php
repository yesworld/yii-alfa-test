<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class ChangePasswordForm extends Model
{

    public $old_password;
    public $new_password;
    public $new_confirm_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_password','new_password','new_confirm_password' ], 'filter', 'filter' => 'trim'],
            [[ 'old_password','new_password','new_confirm_password' ], 'required', 'message' => 'Обязательное поле для заполнения'],
            ['old_password', 'validateCurrentPassword', 'message' => 'Не правильный старый пароль.'],

            [[ 'old_password','new_password','new_confirm_password' ], 'string', 'min' => 4, 'max' => 10,
                'tooLong' => 'Поле не должно быть больше {max} символов',
                'tooShort'=>'Поле не должно быть меньше {min} символов'],

            ['new_confirm_password', 'compare', 'compareAttribute'=>'new_password', "message"=>"Пароли не совпадают."]
        ];
    }

    /**
     * Валидация старого пароля
     */
    public function validateCurrentPassword( $attribute, $params ){
        if( \Yii::$app->user->isGuest ) return false; //На всякий )

        $user = $this->getUser();
        if( !$user ) {
            return $this->addError($attribute, 'Пользователь не найден в БД');
        }

        if( !$user->validatePassword( $this->old_password ) ){
            return $this->addError($attribute, 'Не правильный старый пароль.');
        }
    }


    public function attributeLabels() {
        return [
            'old_password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'new_confirm_password' => 'Подтверждение нового пароля',
        ];
    }


    public function savePassword() {
        $user = $this->getUser();
        $user->setPassword( $this->new_password );
        return $user->save( false );
    }

    protected function getUser(){
        return User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => \Yii::$app->user->identity->email
        ]);

    }
}

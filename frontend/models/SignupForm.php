<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $login;
    public $username;
    public $email;
    public $password;
    public $confirm_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'login'], 'filter', 'filter' => 'trim'],
            [['login', 'email', 'password', 'confirm_password'], 'required', 'message' => 'Обязательное поле для ввода'],

            [['login', 'username'], 'string', 'min' => 4, 'max' => 255,
                'tooLong' => 'Поле не должно быть больше {max} символов',
                'tooShort'=>'Поле не должно быть меньше {min} символов'],

            ['email', 'email', 'message' => 'Не верный формат почты'],
            ['email', 'string', 'max' => 255, 'tooLong' => 'Поле не должно быть больше {max} символов',],

            [['email', 'login'], 'unique',
                'targetClass' => '\common\models\User',
                'message' => 'Этот {value} уже существует.'
            ],

            ['password', 'string', 'min' => 4, 'max'=>10,
                'tooLong' => 'Поле не должно быть больше {max} символов',
                'tooShort'=>'Поле не должно быть меньше {min} символов'
            ],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', "message"=>"Пароли не совпадают."]
        ];
    }

    //Атрибуты для формы
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'confirm_password' => 'Подтвердите Пароль',
            'email' => 'Почта'
        ];
    }

    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->login = $this->login;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
